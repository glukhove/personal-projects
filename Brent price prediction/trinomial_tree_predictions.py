
# coding: utf-8

# In[1]:

import sys
from random import *
import xlwt
from math import fabs 




if __name__ == "__main__":


    dictBrent = {}
    dictCurrency = {}
    arrBrent = []
    arrCurrency = []
    rep = 1000
    ti = 365
    cor = -0.861256089
    a22 = 0.508171181
    currencyAv = 57.6575
    brentSd = 2
    brentAv = 66.87
    lowBrent = 0.16
    middleBrent = 0.84
    deltaBrent = 2.838683539
    lowCurrency = 0.16
    middleCurrency = 0.84
    deltaCurrency = 1.238287959
    arrZ = []

    for i in range(rep):
        brent = brentAv
        currency = currencyAv
        
        for j in range(ti):
            brentRd = normalvariate(0, 1)
            currencyRd = normalvariate(0, 1)
            z1 = brentRd
            z2 = brentRd*cor+currencyRd*a22
            
            
            if z1 < -0.96845893:
                brent = brent - deltaBrent
            elif -0.96845893 <z1 < 0.966385242:
                brent = brent
            else:
                brent = brent + deltaBrent

            if z2 < -0.967526526:
                currency = currency - deltaCurrency
            elif -0.967526526< z2 < 0.967316617:
                currency = currency
            else:
                currency = currency + deltaCurrency

        if brent > 0 and currency > 0:
	        arrBrent.append(brent)
	        arrCurrency.append(currency)
	        if brent in dictBrent:
	            dictBrent[brent] += 1
	        else:
	            dictBrent[brent] = 1
	            
	        if currency in dictCurrency:
	            dictCurrency[currency] += 1
	        else:
	            dictCurrency[currency] = 1
                   

    file = "results_" + str(brentAv) + "_limits.xls"
    workbook = xlwt.Workbook(file)
    arrList = workbook.add_sheet("array")
    dictListBrent = workbook.add_sheet("probabilityBrent")
    dictListCurrency = workbook.add_sheet("probabilityCurrency")
    

    for i in range(len(arrBrent)):
        arrList.write(i, 0, arrBrent[i])
        arrList.write(i, 1, arrCurrency[i])
    for i in range(len(dictBrent)):
        dictListBrent.write(i, 0, list(dictBrent.keys())[i])
        dictListBrent.write(i, 1, list(dictBrent.values())[i])

    for i in range(len(dictCurrency)):
        dictListCurrency.write(i, 0, list(dictCurrency.keys())[i])
        dictListCurrency.write(i, 1, list(dictCurrency.values())[i])


    workbook.save(file)

